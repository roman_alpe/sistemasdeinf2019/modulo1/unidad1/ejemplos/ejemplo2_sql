﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 4 tablas
DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE b20190529;
USE b20190529;

/*
  creando la tabla clientes
*/


CREATE TABLE clientes(
dni int AUTO_INCREMENT,
nombre varchar(100),
tfno varchar(12),
fechaNac date,    
PRIMARY KEY(dni) -- creando la clave
);
CREATE TABLE productos(
cod_pr int AUTO_INCREMENT,
nombre varchar(100),
precio float,
PRIMARY KEY(cod_pr) -- creando la clave
);
CREATE TABLE proveed(
cif int AUTO_INCREMENT,
nomb varchar(100),
direcc varchar(100),
PRIMARY KEY(cif) -- creando la clave
);
CREATE TABLE compra(
  dni int,
  cod_pr int,
  fecha_Nac date,
  PRIMARY KEY(dni,cod_pr), -- creando la clave
  CONSTRAINT fkcompraclientes FOREIGN KEY (dni)
  REFERENCES clientes(dni),
  CONSTRAINT fkcompraproductos FOREIGN KEY(cod_pr)
  REFERENCES productos(cod_pr)

);
CREATE TABLE suministra(
  cif int,
  cod_pr int,
  nombre varchar (100),
  dir varchar (100),
  PRIMARY KEY(cif,cod_pr), -- creando la clave
  CONSTRAINT fksuministraproductos FOREIGN KEY(cod_pr)
  REFERENCES productos(cod_pr),
  CONSTRAINT fksuministraproveed FOREIGN KEY(cif)
  REFERENCES proveed(cif)
  
);
